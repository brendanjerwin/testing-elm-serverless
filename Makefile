.PHONY: server test clean update_build_image test_fmt test_terraform deploy

build: .
	elm package install --yes
	elm make ./src/Main.elm --output=elm.js

clean:
	git clean -xfd
	rm -rf elm-stuff
	rm -rf tests/elm-stuff

server:
	elm live src/Main.elm --host=dev-box.local --pushstate --output=elm.js

test:
	elm test

test_fmt:
	elm format . --validate

test_terraform:
	cd ./deploy; terraform remote config -backend=S3 -backend-config="bucket=akiajqtgshxaoucgz6va.terraform" -backend-config="key=tf-state" -backend-config="region=us-east-2"
	cd ./deploy; terraform validate
	cd ./deploy; terraform plan

update_build_image:
	docker build -t brendanjerwin/elm-ci -f ./ci_build/ElmDockerfile .
	docker push brendanjerwin/elm-ci
	docker build -t brendanjerwin/terraform-ci -f ./ci_build/TerraformDockerfile .
	docker push brendanjerwin/terraform-ci

deploy:
	cd ./deploy; terraform remote config -backend=S3 -backend-config="bucket=akiajqtgshxaoucgz6va.terraform" -backend-config="key=tf-state" -backend-config="region=us-east-2"
	cd ./deploy; terraform get
	cd ./deploy; terraform apply
	cd ./deploy; ./upload.sh
