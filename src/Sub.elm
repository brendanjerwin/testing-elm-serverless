module Sub exposing (..)

import Model exposing (..)
import Msg exposing (..)


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
