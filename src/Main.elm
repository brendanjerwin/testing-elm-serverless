module Main exposing (..)

import Html.App
import Model exposing (..)
import Msg exposing (..)
import View exposing (..)
import Sub exposing (..)


init : ( Model, Cmd Msg )
init =
    ( { changed = False
      }
    , Cmd.none
    )


main : Program Never
main =
    Html.App.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
