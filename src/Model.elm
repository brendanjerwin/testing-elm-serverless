module Model exposing (..)


type alias Model =
    { changed : Bool
    }
