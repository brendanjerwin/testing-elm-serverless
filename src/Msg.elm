module Msg exposing (..)

import Model exposing (..)


type Msg
    = NoOp
    | DoIt


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        DoIt ->
            ( { model | changed = not model.changed }, Cmd.none )
