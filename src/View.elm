module View exposing (view)

import Model exposing (..)
import Msg exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


changedButton : Bool -> Html Msg
changedButton changed =
    button [ onClick DoIt ] [ text "Do It" ]


message : Model -> String
message model =
    if model.changed then
        "Changed"
    else
        "Hello"


view : Model -> Html Msg
view model =
    div []
        [ div [ class "page-header" ] []
        , div [ class "alert alert-success" ]
            [ text (message model)
            , changedButton model.changed
            ]
        ]
