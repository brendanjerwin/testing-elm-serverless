import * as lambda from "aws-lambda";
import * as generate from "./generate";

interface PingEvent {
    readonly name?: string;
}

export function handler(evt: PingEvent | null | undefined,
    ctx: lambda.Context,
    callback: lambda.Callback) {

    function err(msg: string) {
        let errObj = new Error(msg);
        callback(errObj);
        ctx.fail(errObj.message);
    }

    if (!evt) {
        err("No event object provided.");
        return;
    }

    if (!evt.name) {
        err("`name` missing.");
        return;
    }

    callback(undefined, { message: generate.hello(evt.name) });

    ctx.succeed(`Pinged ${evt.name}.`);
}
