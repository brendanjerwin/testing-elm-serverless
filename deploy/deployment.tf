provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

resource "aws_route53_record" "domain-a" {
  zone_id = "${var.hosted_zone_id}"
  name    = "${var.third_level_domain}.brendanjerwin.click"
  type    = "A"

  alias {
    name                   = "${aws_s3_bucket.site.website_domain}"
    zone_id                = "${aws_s3_bucket.site.hosted_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_s3_bucket" "site" {
  bucket = "${var.third_level_domain}.brendanjerwin.click"
  acl    = "public-read"

  policy = <<EOF
{
"Id": "bucket_policy_site",
"Version": "2012-10-17",
"Statement": [
{
"Sid": "bucket_policy_site_main",
"Action": [
"s3:GetObject"
],
"Effect": "Allow",
"Resource": "arn:aws:s3:::${var.third_level_domain}.brendanjerwin.click/*",
"Principal": "*"
}
]
}
EOF

  website {
    index_document = "index.html"
    error_document = "index.html"
  }

  tags {}

  force_destroy = true
}

output "site_s3_uri" {
  value = "s3://${aws_s3_bucket.site.id}"
}
