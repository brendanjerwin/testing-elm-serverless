variable "access_key" {}

variable "secret_key" {}

variable "region" {
  default = "us-east-1"
}

variable "hosted_zone_id" {}

variable "third_level_domain" {
  default = "stack-test"
}
