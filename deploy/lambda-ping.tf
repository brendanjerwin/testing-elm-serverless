resource "aws_lambda_function" "lambda-ping" {
  filename         = "../backend/lambda/ping/build/src.zip"
  source_code_hash = "${base64sha256(file("../backend/lambda/ping/build/src.zip"))}"
  function_name    = "ping"
  role             = "${aws_iam_role.iam_for_lambda.arn}"
  handler          = "main.handler"

  runtime     = "nodejs4.3"
  memory_size = "128"
  timeout     = "3"
}
